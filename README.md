# RecipeApp

## Commande GIT

### git add .

Ajoute les fichiers qui ont été modifiés dans le HEAD

### git commit -m "\<message\>"

- Ajoute les fichiers du HEAD dans un commit. 
- -m est pour un message

### git pull origin \<branch\>

Prends toutes les modifications qui sont en ligne et les importe. On s'assure de ne pas créer de problème de cette façon. La branch est **dev** pour le développement.

### git push origin \<branch\>

Pousse toutes les modifications dans le GIT


### Plus d'info (Cheatsheet)
http://ndpsoftware.com/git-cheatsheet.html#loc=local_repo;