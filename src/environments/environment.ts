// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCUOgMflvkScrfxXPbXEjcpr-A27g5xRGo",
    authDomain: "mealprep-5138d.firebaseapp.com",
    databaseURL: "https://mealprep-5138d.firebaseio.com",
    projectId: "mealprep-5138d",
    storageBucket: "mealprep-5138d.appspot.com",
    messagingSenderId: "528030033127"
  }
};
