export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCUOgMflvkScrfxXPbXEjcpr-A27g5xRGo",
    authDomain: "mealprep-5138d.firebaseapp.com",
    databaseURL: "https://mealprep-5138d.firebaseio.com",
    projectId: "mealprep-5138d",
    storageBucket: "mealprep-5138d.appspot.com",
    messagingSenderId: "528030033127"
  }
};
